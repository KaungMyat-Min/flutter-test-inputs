import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<MyApp> {
  Map<String, bool> _interest = {
    "male": false,
    "female": false,
  };

  List<String> _gender = ["male", "femal"];

  Map<String, bool> _notiType = {
    "email": false,
    "sms": false,
  };

  String _genderValue = "";
  double _weight = 0.0;
  String _bod = "";

  var _nameController = TextEditingController();
  var _ageController = TextEditingController();
  var _emailContolller = TextEditingController();

  _save() {
    print("name: " + _nameController.text);
    print("age: " + _ageController.text);
    print("email: " + _emailContolller.text);
    print("bod: " + _bod);
  }

  _clear() {
    _nameController.clear();
    _ageController.clear();
    _emailContolller.clear();
    _bod = "";
    setState(() {
      _interest.keys.forEach((key) => _interest[key] = false);
      _genderValue = "";
      _notiType.keys.forEach((key) => _notiType[key] = false);
    });
  }

  Widget _createInterest() {
    List<Widget> children = new List();
   // children.add(Text("Tell us what you interset"));

    children.addAll(_interest.keys.map((String key) {
      return CheckboxListTile(
        value: _interest[key],
        title: Text(key),
        onChanged: (interest) => _changeInterest(interest, key),
      );
    }));

    return Column(
      children: children,
    );
  }

  _changeInterest(bool interest, String key) {
    setState(() {
      _interest[key] = interest;
    });
  }

  Widget _createGender() {
    List<Widget> children = new List();
   // children.add(Text("Choose gender"));
    children.addAll(_gender.map((gender) {
      return RadioListTile(
        value: gender,
        groupValue: _genderValue,
        onChanged: _changeGender,
        controlAffinity: ListTileControlAffinity.trailing,
        title: Text(gender),
      );
    }));
    return Column(
      children: children,
    );
  }

  _changeGender(String gender) {
    setState(() {
      _genderValue = gender;
    });
  }

  Widget _createNotificationChoice() {
    List<Widget> children = new List();
    children.addAll(_notiType.keys.map((String key) {
      return SwitchListTile(
        value: _notiType[key],
        title: Text(key),
        onChanged: (status) => _onChangeNoti(key, status),
      );
    }));

    return Column(children: children);
  }

  void _onChangeNoti(String key, bool status) {
    setState(() {
      _notiType[key] = status;
    });
  }

  Widget createWeight() {
    List<Widget> children = new List();
    //children.add(Text("Weight"));
    children.add(Text((_weight * 300).floor().toString()+" lb"));
    children.add(Slider(value: _weight, onChanged: _changeWeight));
    return Column(
      children: children,
    );
  }

  void _changeWeight(double value) => setState(() => _weight = value);

  Future _selectBod() async {
    DateTime bod = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1988),
        lastDate: DateTime(2020)
    );


    if (bod != null) {
      setState(() {
        _bod = bod.toString();
      });
    }
  }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: new Text("Some Form"),
        ),
        body: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.people),
                    hintText: "Kaung Myat Min",
                    labelText: "name:"),
                controller: _nameController,
              ),
              TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.confirmation_number),
                    hintText: "25",
                    labelText: "age:"),
                controller: _ageController,
              ),
              TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.email),
                    hintText: "kaungmyatmin21@gmail.com",
                    labelText: "email"),
                controller: _emailContolller,
              ),
              _createInterest(),
              _createGender(),
              _createNotificationChoice(),
              createWeight(),
              FlatButton(onPressed: _selectBod,child: Text("select Birthday"),),
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: _clear,
                  ),
                  FlatButton(
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.save),
                        Text("Submit"),
                      ],
                    ),
                    onPressed: _save,
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }
